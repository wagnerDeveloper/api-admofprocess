import {useCallback, useEffect, useState} from 'react';
import Body from '../template/Body';
import Header from '../template/Header';
import Page from '../template/Page';
import List from './List';
import Forms from './Form';
import NotFound from '../template/NotFound';
import Id from '@/src/data/logic/core/comum/id.random/Id';
import {Autocomplete, Button} from '@mantine/core';
import {IconForms, IconPlus, IconSearch} from '@tabler/icons-react';
import TitlePage from '../template/TitlePage';
import ICategory, {
	newCategoryInit,
} from '@/src/data/logic/core/category/Category.Interface';
import categoryMock from '@/src/data/constants/category.mock';
import useCategory from '@/src/data/hooks/useCategory';

export default function Category() {
	const {
		listCategories,
		categories,
		save,
		remove,
		select,
		searchTerm,
		category,
		setSearchTerm,
	} = useCategory();

	return (
		<Page>
			<Header />
			<TitlePage
				className="flex justify-center items-center py-4"
				icone={<IconForms />}
				mainTitle="Categorias"
				subTitle={''}
			/>
			<Body className="gap-5">
				<div className="flex justify-between gap-4">
					<div
						className={`flex border border-zinc-600 justify-between gap-4 rounded-xl `}
					>
						<div className="flex items-center scroll-p-0.5 gap-1 rounded-xl bg-zinc-500 ">
							<IconSearch size={28} />
							<Autocomplete
								placeholder="Consultar Categoria"
								onChange={(value) => setSearchTerm(value)}
								value={searchTerm}
								data={[]}
							/>
						</div>
					</div>
					<Button
						className="bg-blue-500"
						leftIcon={<IconPlus />}
						onClick={() => select(newCategoryInit)}
					>
						Adicionar Categorias
					</Button>
				</div>
				{category ? (
					<Forms
						category={category}
						save={save}
						cancel={() => select(null)}
						delete={remove}
					/>
				) : categories.length ? (
					<List categories={categories} selectCategory={select} />
				) : (
					<NotFound>Nenhuma categoria encontrada</NotFound>
				)}
			</Body>
		</Page>
	);
}
