import ICategory from '@/src/data/logic/core/category/Category.Interface';
import Money from '@/src/data/logic/utils/Money';
import {IconTrendingDown, IconTrendingUp} from '@tabler/icons-react';

interface IListCategory {
	categories: ICategory[];
	selectCategory?: (categories: ICategory) => void;
}

export default function List(props: IListCategory) {
	// function renderType(category: ICategory) {
	// 	return (
	// 		<span
	// 			className={`
	//         flex justify-center items-center h-2 w-4 sm:w-10 sm:h-10 p-1.5 rounded-full
	//         ${category.status === 'ativo' ? 'bg-green-500' : 'bg-red-500'}
	//         `}
	// 		>
	// 			{category.status === 'ativo' ? <IconTrendingUp /> : <IconTrendingDown />}
	// 		</span>
	// 	);
	// }

	function renderList(category: ICategory, indice: number) {
		return (
			<div
				key={category.id}
				className={`
            flex items-center gap-3 p-3 cursor-pointer
            ${indice % 2 === 0 ? 'bg-zinc-900' : 'bg-zinc-800'}
            `}
				onClick={() => props.selectCategory?.(category)}
			>
				{/* {renderType(category)} */}
				<span className="w-full md:w-1/2">{category.name}</span>
			</div>
		);
	}

	function renderTitle() {
		return (
			<div
				className={`flex items-start p-3 justify-between  flex-row bg-black text-zinc-400`}
			>
				<div> Nome </div>
			</div>
		);
	}

	return (
		<div
			className={`
            flex flex-col border border-zinc-700 rounded-xl overflow-hidden
        `}
		>
			{renderTitle()}
			{props.categories.map(renderList)}
		</div>
	);
}
