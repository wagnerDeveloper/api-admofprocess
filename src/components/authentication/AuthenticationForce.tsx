import AuthContext from '@/src/data/contexts/AuthenticationContext';
import {useContext} from 'react';
import Landing from '../landing';

interface IAuthenticationForceProps {
	children: any;
}

export default function AuthenticationForce(props: IAuthenticationForceProps) {
	const {user, loading} = useContext(AuthContext);

	if (typeof window !== 'undefined') {
		const router = require('next/router').useRouter();
		if (loading) {
			return <Landing />;
		} else if (user?.email) {
			return props.children;
		} else {
			router.push('/');
			return <Landing />;
		}
	}
}
