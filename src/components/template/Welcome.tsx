// https://unicode-table.com/en/1F44B/
import usuario from '@/src/data/constants/user.mock';
import AuthContext from '@/src/data/contexts/AuthenticationContext';
import {useContext} from 'react';

export default function Welcome() {
	const {user} = useContext(AuthContext);

	function renderizarNome() {
		return <span className="sm:inline-block">{user?.name?.split(' ')[0]}</span>;
	}

	return <div className={`text-3xl font-black`}>Olá {renderizarNome()} 👋</div>;
}
