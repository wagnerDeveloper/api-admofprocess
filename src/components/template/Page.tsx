import AuthenticationForce from '../authentication/AuthenticationForce';

interface PageProps {
	external?: boolean;
	children: any;
	className?: string;
}
export default function Page(props: PageProps) {
	function renderPage() {
		return (
			<div
				className={`
				flex flex-col min-h-screen
				bg-gradient-to-r from-fuchsia-900
				via-black to-fuchsia-900
				${props.className ?? ''}
			`}
			>
				{props.children}
			</div>
		);
	}

	return props.external ? (
		renderPage()
	) : (
		<AuthenticationForce>{renderPage()}</AuthenticationForce>
	);
}
