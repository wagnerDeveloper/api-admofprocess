import {Avatar, Menu} from '@mantine/core';
import {
	IconCategory,
	IconChecklist,
	IconLogout,
	IconUser,
} from '@tabler/icons-react';
import Link from 'next/link';
import {useContext} from 'react';
import AuthContext from '@/src/data/contexts/AuthenticationContext';

export default function MenuUser() {
	const {user, logout} = useContext(AuthContext);

	return (
		<Menu>
			<Menu.Target>
				<div className="flex items-center gap-3 cursor-pointer">
					<div className="hidden md:flex flex-col select-none">
						<span className="text-sm font-bold text-zinc-200">
							{user?.name}
						</span>
						<span className="text-xs text-zinc-400">{user?.email}</span>
					</div>
					<Avatar
						size={40}
						radius="xl"
						src={
							user?.imagemUrl ??
							'https://source.unsplash.com/random/100x100/?abstract'
						}
					/>
				</div>
			</Menu.Target>
			<Menu.Dropdown>
				<Menu.Label>Menu</Menu.Label>
				<Menu.Divider />
				<Link href="/user">
					<Menu.Item icon={<IconUser size={14} />}>Usuários</Menu.Item>
				</Link>
				<Link href="/">
					<Menu.Item icon={<IconChecklist size={14} />}>Processos</Menu.Item>
				</Link>
				<Link href="/category">
					<Menu.Item icon={<IconCategory size={14} />}>Categorias</Menu.Item>
				</Link>
				<Menu.Divider />
				<Link href="/">
					<Menu.Item
						color="red"
						icon={<IconLogout size={14} />}
						onClick={logout}
					>
						Sair
					</Menu.Item>
				</Link>
			</Menu.Dropdown>
		</Menu>
	);
}
