import useStateForm from '@/src/data/hooks/useUser';
import IProcess from '@/src/data/logic/core/process/Process.Interface';
import {StatusTypeProcess} from '@/src/data/logic/core/status-type/status.type';
import {Button, Group, Radio, Select, TextInput, Textarea} from '@mantine/core';
import {DatePickerInput} from '@mantine/dates';
import {useState} from 'react';

interface IFormsProps {
	process: IProcess;
	save?: (process: IProcess) => void;
	delete?: (process: IProcess) => void;
	cancel?: () => void;
}

export default function Forms(props: IFormsProps) {
	const {data, changeAttributes} = useStateForm<IProcess>(props.process);

	return (
		<div className="flex flex-col border border-zinc-700 rounded-xl overflow-hidden">
			<div className="bg-black py-3 px-7 text-zinc-400">Formulário</div>
			<div className=" flex flex-col justify-between bg-zinc-500 gap-4 p-4 sm:p-7">
				<Group className="flex flex-row justify-between p-1">
					<TextInput
						label="Núm. Processo"
						value={data.numProcess}
						onChange={changeAttributes('numProcess')}
					></TextInput>
					<DatePickerInput
						label="Data SAJ"
						value={data.dateSaj}
						locale="pt-BR"
						valueFormat="DD/MM/YYYY"
						onChange={changeAttributes('dateSaj')}
					/>
					<DatePickerInput
						label="Prazo Final"
						value={data.deadline}
						locale="pt-BR"
						valueFormat="DD/MM/YYYY"
						onChange={changeAttributes('deadline')}
					/>

					<DatePickerInput
						label="Data Relatório"
						value={data.report}
						locale="pt-BR"
						valueFormat="DD/MM/YYYY"
						onChange={changeAttributes('report')}
					/>

					<DatePickerInput
						label="Data Audiência"
						value={data.courtHearing}
						locale="pt-BR"
						valueFormat="DD/MM/YYYY"
						onChange={changeAttributes('courtHearing')}
					/>
				</Group>
				<Group grow>
					<Select
						label="Vara"
						placeholder="Vara"
						data={['Família', 'Criminal']}
					/>
					<Select
						label="Ação"
						placeholder="Ação"
						data={['Visita', 'Reunição']}
					/>
					<Select
						label="Fila Social/Psicológico"
						placeholder="Fila Social/Psicológico"
						data={['Ass.1', 'Ass.2']}
					/>
				</Group>
				<TextInput
					label="Partes"
					value={data.parts ?? ' N/A'}
					onChange={changeAttributes('parts')}
				></TextInput>
				<TextInput
					label="Crianças/Idosos"
					value={data.childrenElderly}
					onChange={changeAttributes('childrenElderly')}
				></TextInput>
				<TextInput
					label="Quesitos"
					value={data.question ?? ' N/A'}
					onChange={changeAttributes('question')}
				></TextInput>
				<Radio.Group value={data.status} onChange={changeAttributes('status')}>
					<Group>
						<Radio value={StatusTypeProcess.OPEN} label="Aberto" />
						<Radio value={StatusTypeProcess.PROGRESS} label="Em Andamento" />
						<Radio value={StatusTypeProcess.CLOSED} label="Finalizado" />
					</Group>
				</Radio.Group>
			</div>
			<div className="flex px-4 sm:px-7 py-4 gap-3 bg-zinc-800">
				<Button
					className="bg-green-500"
					color="greee"
					onClick={() => props.save?.(data)}
				>
					Salvar
				</Button>
				<Button className="bg-zinc-500" color="gray" onClick={props.cancel}>
					Voltar
				</Button>
				<div className="flex-1"> </div>
				{data.id && (
					<Button
						className="bg-red-500"
						color="red"
						onClick={() => props.delete?.(data)}
					>
						Excluir
					</Button>
				)}
			</div>
		</div>
	);
}
