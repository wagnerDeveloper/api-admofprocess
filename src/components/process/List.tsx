import IProcess from '@/src/data/logic/core/process/Process.Interface';
import Data from '@/src/data/logic/utils/Data';

import {
	IconClockPlay,
	IconClockStop,
	IconDiscountCheck,
} from '@tabler/icons-react';

interface IListProcess {
	allProcess: IProcess[];
	selectProcess?: (process: IProcess) => void;
}

export default function List(props: IListProcess) {
	function renderType(process: IProcess) {
		return (
			<span
				className={`
				flex justify-center items-center
				h-8 w-8 sm:w-10 sm:h-10 p-1.5 rounded-full            
			${
				process.status === 'aberto'
					? 'bg-yellow-500 '
					: process.status === 'andamento'
					? 'bg-orange-500'
					: 'bg-green-500'
			}  
			`}
				title={process.status}
			>
				{process.status === 'aberto' ? (
					<IconClockStop />
				) : process.status === 'andamento' ? (
					<IconClockPlay />
				) : (
					<IconDiscountCheck />
				)}
			</span>
		);
	}

	function renderList(process: IProcess, indice: number) {
		return (
			<div
				key={process.id}
				className={`
            flex items-center justify-between gap-3 p-3 cursor-pointer
            ${indice % 2 === 0 ? 'bg-zinc-500' : 'bg-zinc-800'}
            `}
				onClick={() => props.selectProcess?.(process)}
			>
				<span>{renderType(process)}</span>
				<span>{process.numProcess}</span>
				<span>{Data.ddmmyy.format(process.dateSaj)}</span>
				<span className="hidden md:inline">
					{Data.ddmmyy.format(process.deadline)}
				</span>
			</div>
		);
	}

	function renderTitle() {
		return (
			<div
				className={`flex items-start p-3 justify-between  flex-row bg-black text-zinc-400`}
			>
				<div> Status </div>
				<div> Num Processo </div>
				<div> Data SAJ </div>
				<div className="hidden md:inline">Prazo Final</div>
			</div>
		);
	}
	return (
		<div
			className={`
            flex flex-col border  border-zinc-700 rounded-xl overflow-hidden
        `}
		>
			{renderTitle()}
			{props.allProcess.map(renderList)}
		</div>
	);
}
