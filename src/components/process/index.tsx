import {useCallback, useContext, useEffect, useState} from 'react';
import Body from '../template/Body';
import Header from '../template/Header';
import Page from '../template/Page';
import List from './List';

import Forms from './Form';
import NotFound from '../template/NotFound';
import {Autocomplete, Button} from '@mantine/core';
import {IconForms, IconPlus, IconSearch} from '@tabler/icons-react';
import TitlePage from '../template/TitlePage';
import useMovement from '@/src/data/hooks/useMovement';
import {newProcessInit} from '@/src/data/logic/core/process/Process.Interface';

export default function Process() {
	const {
		allProcesses,
		process,
		save,
		remove,
		select,
		setSearchTerm,
		searchTerm,
	} = useMovement();

	return (
		<Page>
			<Header />
			<TitlePage
				className="flex justify-center items-center py-4"
				icone={<IconForms />}
				mainTitle="Gestão Eficiente de Processos"
				subTitle={''}
			/>
			<Body className="gap-5">
				<div className="flex justify-between gap-4">
					<div
						className={`flex border border-zinc-600 justify-between gap-4 rounded-xl `}
					>
						<div className="flex items-center scroll-p-0.5 gap-1 rounded-xl bg-zinc-500 ">
							<IconSearch size={28} />
							<Autocomplete
								placeholder="Consultar Num Processo"
								onChange={(value) => setSearchTerm(value)}
								value={searchTerm}
								data={[]}
							/>
						</div>
					</div>
					<Button
						className="bg-blue-500"
						leftIcon={<IconPlus />}
						onClick={() => select(newProcessInit)}
					>
						Adicionar Processo
					</Button>
				</div>
				{process ? (
					<Forms
						process={process}
						save={save}
						cancel={() => select(null)}
						delete={remove}
					/>
				) : allProcesses.length ? (
					<List allProcess={allProcesses} selectProcess={select} />
				) : (
					<NotFound>Nenhum produto encontrado</NotFound>
				)}
			</Body>
		</Page>
	);
}
