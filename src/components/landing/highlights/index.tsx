import ImagemResponsiva from '../comum/ImagemResponsiva';
import principal from '@/public/girl-doing-video-calling.svg';
import Slogan from './Slogan';
import Area from '../comum/Area';

export default function Highlights() {
	return (
		<Area id="start" className="pt-20">
			<div
				className={`
				flex items-center
				justify-around
				
				h-[550px]
			`}
			>
				<Slogan />
				<ImagemResponsiva imagem={principal} className=" hidden md:inline" />
			</div>
		</Area>
	);
}
