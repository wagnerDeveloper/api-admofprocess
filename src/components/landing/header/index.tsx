import Logo from '../comum/Logo';
import Menu from './Menu';
import Area from '../comum/Area';

export default function Header() {
	return (
		<Area className={`bg-fuchsia-900 fixed z-50`}>
			<div className="flex items-center justify-between h-20">
				<Logo />
				<Menu />
			</div>
		</Area>
	);
}
