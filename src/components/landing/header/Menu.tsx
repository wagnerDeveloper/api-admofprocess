// import {useContext} from 'react';
import Link from 'next/link';
import MenuItem from './MenuItem';
import {IconBrandGoogle} from '@tabler/icons-react';
import {useContext} from 'react';
import AuthContext from '@/src/data/contexts/AuthenticationContext';

export default function Menu() {
	const {loginGoogle} = useContext(AuthContext);

	return (
		<div className="flex gap-2">
			{/* <MenuItem url="#start" className="hidden sm:flex">
				Início
			</MenuItem>
			<MenuItem url="#advantage" className="hidden sm:flex">
				Vantagens
			</MenuItem>
			<MenuItem url="#highlights" className="hidden sm:flex">
				Destaques
			</MenuItem> */}
			<MenuItem
				onClick={loginGoogle}
				className="bg-gradient-to-r from-red-600 to-red-500"
			>
				<div className="flex items-center gap-2">
					<IconBrandGoogle size={15} />
					<span>Login</span>
				</div>
			</MenuItem>
		</div>
	);
}
