export default function Logo() {
	return (
		<div className="text-3xl">
			<span className="font-bold">Juris</span>
			<span className="text-zinc-400 font-thin">FLOW</span>
		</div>
	);
}
