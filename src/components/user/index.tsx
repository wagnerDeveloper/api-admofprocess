import {IconForms} from '@tabler/icons-react';
import Header from '../template/Header';
import Page from '../template/Page';
import TitlePage from '../template/TitlePage';
import Body from '../template/Body';
import {useContext} from 'react';
import AuthContext from '../../data/contexts/AuthenticationContext';

export default function User() {
	const {user} = useContext(AuthContext);
	return (
		<Page>
			<Header />
			<TitlePage
				className="flex justify-center items-center py-4"
				icone={<IconForms />}
				mainTitle="Dados Cadastrais de usuário"
				subTitle={`Informações de ${user?.email}`}
			/>
			<Body className="gap-5">Usuário</Body>
		</Page>
	);
}
