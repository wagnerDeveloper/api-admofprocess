import '@/src/styles/globals.css';
import type {AppProps} from 'next/app';
import {MantineProvider} from '@mantine/core';
import {AuthenticationWithProvider} from '../data/contexts/AuthenticationContext';

export default function App({Component, pageProps}: AppProps) {
	return (
		<MantineProvider
			theme={{
				colorScheme: 'dark',
			}}
		>
			<AuthenticationWithProvider>
				<Component {...pageProps} />
			</AuthenticationWithProvider>
		</MantineProvider>
	);
}
