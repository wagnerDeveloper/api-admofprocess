import {useContext} from 'react';
import Landing from '../components/landing';
import Process from '../components/process';
import AuthContext from '../data/contexts/AuthenticationContext';
import Loading from '../components/template/Loading';

export default function Home() {
	const {user, loading} = useContext(AuthContext);

	if (loading) return <Loading />;
	return user ? <Process /> : <Landing />;
}
