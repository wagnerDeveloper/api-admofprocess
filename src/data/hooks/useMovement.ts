import {useCallback, useContext, useEffect, useState} from 'react';
import AuthContext from '../contexts/AuthenticationContext';
import services from '../logic/core';
import IProcess from '../logic/core/process/Process.Interface';

export default function useMovement() {
	const {user} = useContext(AuthContext);
	const [data, setData] = useState<Date>(new Date());

	const [allProcesses, setAllProcesses] = useState<IProcess[]>([]);
	const [process, setProcess] = useState<IProcess | null>(null);
	const [searchTerm, setSearchTerm] = useState('');

	const searchProcess = useCallback(async () => {
		if (!user) return;
		try {
			const allProcesses = await services.movement.getProcess(user);			
			setAllProcesses(allProcesses);
		} catch (error) {
			console.error('Failed to fetch processes:', error);
		}
	}, [user]);

	const searchNumProcess = useCallback(() => {
		if (!user) return;

		const filteredProcesses = allProcesses.filter((p) =>
			p.numProcess?.toLowerCase().includes(searchTerm.toLowerCase())
		);

		setAllProcesses(filteredProcesses);
		setProcess(null);
	}, [allProcesses, searchTerm, user]);

	useEffect(() => {
		if (searchTerm) {
			searchNumProcess();
		} else {
			searchProcess();
		}
	}, [searchProcess, data, searchTerm, searchNumProcess]);

	const save = useCallback(
		async (process: IProcess) => {
			if (!user) return;
			try {
				services.movement.save(process, user);
				setProcess(null);
				await searchProcess();
			} catch (error) {
				console.error('Failed to save process:', error);
			}
		},
		[searchProcess, user]
	);

	const remove = useCallback(
		async (process: IProcess) => {
			if (!user) return;
			try {
				await services.movement.delete(process, user);
				setProcess(null);
				await searchProcess();
			} catch (error) {
				console.error('Failed to remove process:', error);
			}
		},
		[searchProcess, user]
	);

	return {
		data,
		allProcesses,
		process,
		save,
		remove,
		select: setProcess,
		updatedAtData: setData,
		setSearchTerm,
		searchTerm,
		searchProcess,
		searchNumProcess,
	};
}
