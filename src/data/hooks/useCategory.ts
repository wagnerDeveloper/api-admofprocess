import {useCallback, useContext, useEffect, useState} from 'react';
import AuthContext from '../contexts/AuthenticationContext';
import ICategory from '../logic/core/category/Category.Interface';
import services from '../logic/core';

export default function useCategory() {
	const {user} = useContext(AuthContext);

	const [categories, setCategories] = useState<ICategory[]>([]);
	const [category, setCategory] = useState<ICategory | null>(null);
	const [searchTerm, setSearchTerm] = useState('');

	const listCategories = useCallback(async () => {
		if (!user) return;
		try {
			const categories = await services.category.getCategory();
			console.log('hooks-cate', categories);
			setCategories(categories);
		} catch (error) {
			console.error('Failed to fetch categories:', error);
		}
	}, [user]);

	const searchNameCategory = useCallback(() => {
		if (!user) return;

		const filteredCategory = categories.filter((c) =>
			c.name?.toLowerCase().includes(searchTerm.toLowerCase())
		);

		setCategories(filteredCategory);
		setCategory(null);
	}, [categories, searchTerm, user]);

	useEffect(() => {
		if (searchTerm) {
			searchNameCategory();
		} else {
			listCategories();
		}
	}, [listCategories, searchTerm, searchNameCategory]);

	const save = useCallback(
		async (category: ICategory) => {
			if (!user) return;
			try {
				services.category.save(category);
				setCategory(null);
				await listCategories();
			} catch (error) {
				console.error('Failed to save category:', error);
			}
		},
		[listCategories, user]
	);

	const remove = useCallback(
		async (category: ICategory) => {
			if (!user) return;
			try {
				await services.category.delete(category);
				setCategory(null);
				await listCategories();
			} catch (error) {
				console.error('Failed to remove category:', error);
			}
		},
		[listCategories, user]
	);

	return {
		listCategories,
		save,
		remove,
		searchNameCategory,
		select: setCategory,
		category,
		categories,
		searchTerm,
		setSearchTerm,
	};
}
