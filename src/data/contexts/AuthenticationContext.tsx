import {createContext, useEffect, useState} from 'react';
import IUser from '../logic/core/users/User.Interface';
import services from '../logic/core';

interface IAuthContextProps {
	loading: boolean;
	user: IUser | null;
	loginGoogle: () => Promise<IUser | null>;
	logout: () => Promise<void>;
	updatedUser: (newUser: IUser) => Promise<void>;
}

const AuthContext = createContext<IAuthContextProps>({
	loading: true,
	user: null,
	loginGoogle: async () => null,
	logout: async () => {},
	updatedUser: async () => {},
});

export function AuthenticationWithProvider(props: any) {
	const [loading, setLoading] = useState<boolean>(false);
	const [user, setUser] = useState<IUser | null>(null);

	useEffect(() => {
		const finishSession = services.user.authenticationMonitoring((user) => {
			setUser(user);
			setLoading(false);
		});
		return finishSession;
	}, []);

	async function loginGoogle() {
		const user = await services.user.loginGoogle();
		setUser(user);
		return user;
	}

	async function logout() {
		await services.user.logout();
		setUser(null);
	}

	async function updatedUser(newUser: IUser) {
		if (user && user.email !== newUser.email) return logout();
		if (user && newUser && user.email === newUser.email) {
			await services.user.saveUser(newUser);
			setUser(newUser);
		}
	}

	return (
		<AuthContext.Provider
			value={{
				loading,
				user,
				loginGoogle,
				logout,
				updatedUser,
			}}
		>
			{props.children}
		</AuthContext.Provider>
	);
}

export default AuthContext;
