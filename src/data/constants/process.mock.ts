import Id from '../logic/core/comum/id.random/Id';
import IMovement from '../logic/core/process/Process.Interface';
import {StatusTypeProcess} from '../logic/core/status-type/status.type';

const processMock: IMovement[] = [
	{
		id: Id.novo(),
		dateSaj: new Date(2024, 1, 19),
		deadline: new Date(2024, 1, 29),
		numProcess: '564654.554-65',
		parts: 'Maria Jones Camargo e Carlos Antonio Camargo',
		childrenElderly:
			'José Antonio Camargo (Avô) e Theresa Maria Camargo (Avó) Julia Camargo Jones',
		report: new Date(),
		question: '',
		status: StatusTypeProcess.OPEN,
	},
	{
		id: Id.novo(),
		dateSaj: new Date(2024, 1, 23),
		deadline: new Date(2024, 1, 30),
		numProcess: '564654.589-96',
		parts: 'Tonhão Machado e Maria Canivete',
		childrenElderly: 'João Espinguarda (Filho) ',
		report: new Date(),
		question: 'Casados',
		status: StatusTypeProcess.PROGRESS,
	},
	{
		id: Id.novo(),
		dateSaj: new Date(2024, 1, 10),
		deadline: new Date(2024, 1, 25),
		numProcess: '564654.777-16',
		parts: 'Cauã Goncalves Cunha e Laura Alves Ribeiro',
		childrenElderly: 'Juquinha (Filho) ',
		report: new Date(),
		question: 'Divorcio ',
		status: StatusTypeProcess.CLOSED,
	},
	{
		id: Id.novo(),
		dateSaj: new Date(2024, 1, 29),
		deadline: new Date(2024, 2, 12),
		numProcess: '564657.325-76',
		parts: 'Samuel Costa Araujo',
		childrenElderly: 'Ágatha Silva Souza (Mãe) ',
		report: new Date(),
		question: 'Casado',
		status: StatusTypeProcess.OPEN,
	},
];

export default processMock;
