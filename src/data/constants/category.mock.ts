import ICategory from '../logic/core/category/Category.Interface';
import Id from '../logic/core/comum/id.random/Id';
import {StatusTypeGategory} from '../logic/core/status-type/status.type';

const categoryMock: ICategory[] = [
	{
		id: Id.novo(),
		name: 'VARA',
		status: StatusTypeGategory.ACTIVE,
	},
	{
		id: Id.novo(),
		name: 'AÇÃO',
		status: StatusTypeGategory.ACTIVE,
	},
	{
		id: Id.novo(),
		name: 'FILA SOCI/PSICO',
		status: StatusTypeGategory.ACTIVE,
	},
];

export default categoryMock;
