import IUsuario from '../logic/core/users/User.Interface';
import Id from '../logic/core/comum/id.random/Id';

export default {
	id: Id.novo(),
	name: 'Amanda Daniele',
	email: 'amanda.daniele@gmail.com',
	imagemUrl: null,
} as IUsuario;
