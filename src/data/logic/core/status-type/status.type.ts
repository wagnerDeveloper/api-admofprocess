export enum StatusTypeProcess {
	OPEN = 'aberto',
	PROGRESS = 'andamento',
	CLOSED = 'fechado',
}
export enum StatusTypeGategory {
	ACTIVE = 'ativo',
	INACTIVE = 'inativo',
}
