import {StatusTypeProcess} from '../status-type/status.type';

export default interface IProcess {
	id?: string;
	dateSaj: Date;
	deadline: Date;
	numProcess: string;
	court?: string;
	action?: string;
	queue?: string;
	parts: string;
	childrenElderly?: string;
	report?: Date;
	question?: string;
	courtHearing?: Date;
	status: StatusTypeProcess;
}

export const newProcessInit: IProcess = {
	dateSaj: new Date(),
	deadline: new Date(),
	numProcess: '',
	court: '',
	action: '',
	queue: '',
	parts: '',
	childrenElderly: '',
	report: new Date(),
	question: '',
	status: StatusTypeProcess.OPEN,
};
