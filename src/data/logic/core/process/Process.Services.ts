import Collection from '@/src/infra/firebase/db/Collection';
import IProcess from './Process.Interface';
import IUser from '../users/User.Interface';
import Data from '../../utils/Data';

export default class MovementService {
	private _collection = new Collection();

	async save(process: IProcess, user: IUser) {
		return this._collection.save(`movement/${user.email}/process`, process);
	}

	async delete(process: IProcess, user: IUser) {
		return this._collection.delete(
			`movement/${user.email}/process`,
			process.id
		);
	}

	async getProcess(user: IUser) {
		const path = `movement/${user.email}/process`;
		return await this._collection.get(path, 'dateSaj', 'desc');
	}

	// async getProcessByNumProcess(user: IUser, numProcess: string) {
	// 	const path = `movement/${user.email}/process`;

	// 	const result = await this._collection.getWithFilter(path, [
	// 		{attribute: 'numProcess', op: '==', value: numProcess},
	// 	]);

	// 	console.log('resultNumProcess', result);
	// 	return result;
	// }
}
