import {StatusTypeGategory} from '../status-type/status.type';

export default interface ICategory {
	id?: string;
	name: string;
	status: StatusTypeGategory;
}

export const newCategoryInit: ICategory = {
	name: '',
	status: StatusTypeGategory.ACTIVE,
};
