import Collection from '@/src/infra/firebase/db/Collection';
import ICategory from './Category.Interface';

export default class CategoryService {
	private _collection = new Collection();

	async save(category: ICategory) {
		return this._collection.save(`category`, category);
	}

	async delete(category: ICategory) {
		return this._collection.delete(`category`, category.id);
	}

	async getCategory() {
		const path = `category`;
		const result = await this._collection.get(path, 'name', 'desc');
		console.log('categoryService', result);
		return result;
	}
}
