import CategoryService from './category/Category.Service';
import MovementService from './process/Process.Services';
import UserService from './users/User.Services';

class Services {
	get user() {
		return new UserService();
	}
	get movement() {
		return new MovementService();
	}

	get category() {
		return new CategoryService();
	}
}

const services = new Services();
export default services;
