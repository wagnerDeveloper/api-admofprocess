import Authentication, {
	CancelMonitoring,
	MonitoringUser,
} from '@/src/infra/firebase/auth/Authentication';
import Collection from '@/src/infra/firebase/db/Collection';
import IUser from './User.Interface';

export default class UserService {
	private _authentication = new Authentication();
	private _collection = new Collection();

	authenticationMonitoring(observer: MonitoringUser): CancelMonitoring {
		return this._authentication.monitoring(async (user) => {
			observer(user ? {...user, ...(await this.getUser(user.email))} : null);
		});
	}

	async loginGoogle(): Promise<IUser | null> {
		const user = await this._authentication.loginGoogle();
		if (!user) return null;

		let userExists = await this.getUser(user.email);
		if (!userExists) userExists = await this.saveUser(user);

		return {
			...user,
			...userExists,
		};
	}

	logout(): Promise<void> {
		return this._authentication.logout();
	}

	async saveUser(user: IUser) {
		return await this._collection.save('users', user, user.email);
	}

	async getUser(email: string) {
		return await this._collection.getById('users', email);
	}
}
