import {
	DocumentData,
	DocumentSnapshot,
	OrderByDirection,
	QueryConstraint,
	WhereFilterOp,
	collection,
	deleteDoc,
	doc,
	getDoc,
	getDocs,
	getFirestore,
	orderBy,
	query,
	setDoc,
	where,
} from 'firebase/firestore';
import {app} from '../config/app';
import Id from '@/src/data/logic/core/comum/id.random/Id';

export interface IFilter {
	attribute: string;
	op: WhereFilterOp;
	value: any;
}

export default class Collection {
	async save(path: string, entity: any, id?: string): Promise<any> {
		const db = getFirestore(app);
		const idEnd = id ?? entity.id ?? Id.novo();
		const docRef = doc(db, path, idEnd);
		await setDoc(docRef, entity);

		return {
			...entity,
			id: entity.id ?? idEnd,
		};
	}

	async delete(path: string, id?: string): Promise<boolean> {
		if (!id) return false;
		const db = getFirestore(app);
		const docRef = doc(db, path, id);
		const itemDataBase = await getDoc(docRef);
		if (!itemDataBase.exists()) return false;
		await deleteDoc(docRef);
		return true;
	}

	async get(
		path: string,
		ordenating?: string,
		direction?: OrderByDirection
	): Promise<any[]> {
		const db = getFirestore(app);
		const collectionRef = collection(db, path);
		const filter: QueryConstraint[] = [];
		const ordering = ordenating ? [orderBy(ordenating, direction)] : [];
		const consulting = query(collectionRef, ...filter, ...ordering);
		const result = await getDocs(consulting);
		return result.docs.map(this._convertToFirebase) ?? [];
	}

	async getById(path: string, id: string): Promise<any> {
		if (!id) return null;
		const db = getFirestore(app);
		const docRef = doc(db, path, id);
		const result = await getDoc(docRef);
		return this._convertToFirebase(result);
	}

	async getWithFilter(
		path: string,
		filters: IFilter[],
		ordenating?: string,
		direction?: OrderByDirection
	): Promise<any[]> {
		const db = getFirestore(app);
		const collectionRef = collection(db, path);

		const filterWhere =
			filters?.map((f) => where(f.attribute, f.op, f.value)) ?? [];
		const ordering = ordenating ? [orderBy(ordenating, direction)] : [];
		const consulting = query(collectionRef, ...filterWhere, ...ordering);
		const result = await getDocs(consulting);
		return result.docs.map(this._convertToFirebase) ?? [];
	}

	private _convertToFirebase(snapshot: DocumentSnapshot<DocumentData>) {
		if (!snapshot.exists()) return null;
		const entity: any = {...snapshot.data(), id: snapshot.id};
		if (!entity) return entity;
		return Object.keys(entity).reduce((obj: any, attribute: string) => {
			const value: any = entity[attribute];
			return {...obj, [attribute]: value.toDate?.() ?? value};
		}, {});
	}
}
