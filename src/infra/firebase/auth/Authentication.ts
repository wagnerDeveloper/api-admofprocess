import {
	Auth,
	GoogleAuthProvider,
	User,
	getAuth,
	onIdTokenChanged,
	signInWithPopup,
	signOut,
} from 'firebase/auth';
import {app} from '../config/app';
import IUsuario from '@/src/data/logic/core/users/User.Interface';

export type MonitoringUser = (user: IUsuario | null) => void;
export type CancelMonitoring = () => void;

export default class Authentication {
	private _auth: Auth;

	constructor() {
		this._auth = getAuth(app);
	}

	async loginGoogle(): Promise<IUsuario | null> {
		const resp = await signInWithPopup(this._auth, new GoogleAuthProvider());

		return this.convertToUser(resp.user);
	}

	logout(): Promise<void> {
		return signOut(this._auth);
	}

	monitoring(noify: MonitoringUser): CancelMonitoring {
		return onIdTokenChanged(this._auth, async (userFirebase) => {
			const user = this.convertToUser(userFirebase);
			noify(user);
		});
	}

	private convertToUser(userFirebase: User | null): IUsuario | null {
		if (!userFirebase?.email) return null;
		const alternativeName = userFirebase.email!.split('@')[0];

		return {
			id: userFirebase.uid,
			name: userFirebase.displayName ?? alternativeName,
			email: userFirebase.email,
			imagemUrl: userFirebase.photoURL,
		};
	}
}
